# Projeto SpringBoot - Forum
Curso da Alura

1. Cria o projeto usando o Spring Initializer[https://start.spring.io/], baixa, descompacta e abre no IntelliJ

Nota: Dê uma olhadinha no arquivo pom.xml para ver as definições e dependencias iniciais do projeto

2. Se você rodar a aplicação vai subir um servidor Tomcat na porta 8080 [http://localhost:8080/] - vai aparecer uma mensagem de erro 404 que é normal (não temos o endereço "/" mapeado)

3.  Criamos uma classe controller HelloController para mapear um endereço "/" e imprimir uma mensagem na tela
```
    @Controller
    @RequestMapping("/")
    @ResponseBody
```

4. Importação do pacote modelo contendo as classes de domínio da aplicação (copia arquivos e cola na pasta src)

5. Criando endpoint para devolver a lista de Tópicos (TopicosController)

Nota: jackson converte a lista em um json para exibir no browser (isso ocorre por debaixo dos panos)

6. Simplificar os controllers trocando @Controller e @ ResponseBody por @RestController na classe. Essa alteração dispensa a necessidade do @ResponseBody nos métodos.

7. Adicionar a dependencia do DevTools no pom.xml para facilitar o reload do Spring quando fizermos alterações no código

8. Implementar uma DTO(Data Transfer Object) para não expor todos os dados da classe de domínio (pacote DTO dentro do Controller) -  Só tem tipos primitivos nos campos, construtor(que recebe a classe de domínio e repassa as informações para a classe DTO) e só getters

9. Substitui a lista de topicos do método lista (TopicosController) por uma lista de TopicosDto, ajustando a resposta do método aplicando o método converter de tópico para 
topicoDto na classe controller .

10. Adicionar a camada de dados usando a JPA. Primeiramente adicionando a dependencia data-jpa no pom.xml 

Nota: por padrao o Spring usa o Hibernate como implementacao da JPA mas voce pode alterar se desejar

11. Adicionar dependencias relativas ao banco de dados no arquivo pom.xml (usaremos o h2 que é um banco em memória) e tambem as configuraçoes do banco de dados no arquivo applications.properties

12. Transformar as classes de dominio (model) em entidades da JPA (mapeamento):
- Inserindo a anotation @Entity na classe
- @Id e @GeneratedValue na chave primaria autoincremento
- Anotations de cardinalidade para campos relacionados a outras tabelas (@ManeToOne, @OneTo Many, @ManyToMany)
- @Enumerated para campos Enum (indicando como queremos quardar a informacao no banco)

13. Inserir o arquivo data.sql, contendo comandos de inserção de dados no banco, na pasta resource. Isso faz com que o Spring execute os comandos e popule o banco. 
Nota: Se quiser verificar como está o banco basta acessar o endereço localhost com o endpoint /h2-console

14. Criar repositories (tem o papel da classe DAO, para não expor sua entidade). Basta adicionar uma interface e herdar os métodos básicos para acessoa às informações do banco. 
Para isso crie uma classe com o Repository no nome ou adicione a anotation @Repository e herde da classe JPARepository<Entidade, tipo do atributo do Id>

15. Depois de criar o repository, você injeta a classe no TopicosController(@Autowired) e refatora o método lista() para agora puxar a informação do banco via repository e devolver o TopicosDto

Nota: Precisamos ter um construtor default na classe Topico, como criamos um contrutor personalizado o Java entende que esse é o default, para resolver: apaga o construtor criado e o Java entende que o default é o contrutor vazio. Fazemos o mesmo para a classe Curso.

16. Refatorar o método lista para receber um parâmetro (query parameter) e retornar dados filtrados, caso contrário retornar todos. E adicionar o método findByTitulo() no TopicoRepository

Nota: Nomenclatura padrao findByNomeDoAtributo(); 

Para criar manualmente a consulta com JPQL, devemos utilizar a anotação @Query;

Adicionando o query parameter na url: `http://localhost:8080/topicos?titulo=Dúvida`

17. Refatorada a definição do endpoint para que o endereço seja padronizado na classe, e não no metodo, assim o metodo define apenas o verbo a ser usado (GET,POST,...): @GetMapping e @PostMapping

Diferenciação de DTOs Nomenclatura:
- ClasseDto - dados que saem para o cliente
- ClasseForm - dados que entram digitados pelo cliente
Ambos são DTOs, mas fazemos essa diferenciação para facilitar

18. Criação da classe TopicoForm para definir o formato de dados que serão recebidos do cliente, e adição da anotation @RequestBody para avisar que a informação vem do corpo da requisição POST.

19. Criação da Interface CursoRepository e injeção na classe controller para permitir o acesso aos dados da tabela curso. O cliente passa o nome do curso, mas precisamos recuperar a entidade para inserir no novo topico que está sendo criado.
Logo, criamos um metodo converter que recebe o CursoRepository e envia para que a classe TopicoForm consiga encontrar a entidade curso por nome (usando findByNome);
Também foi preciso criar um construtor na classe topico recebendo os dados passados pelo cliente (e o construtor vazio requerido pela JPA)

Para testar basta:
(Figura da requisição do post)

20. Alterando o status code de retorno de 200 para 201 (created - indica que foi feita requisição e adicionado novo recurso no servidor, mais indicada neste caso).Adicionamos um ResponseEntity como resposta 

Toda vez que devolvo 201 para o cliente, além de devolver o código, tenho que devolver mais duas coisas:   

1)cabeçalho http, chamado location, com a url desse novo recurso que acabou de ser criado. 
2)corpo da resposta eu tenho que devolver uma representação desse recurso que acabei de criar.

Usamos o UriComponentsBuilder pra gerar um path relativo, que fica mais fãcil de manter quando mudar o endereço do nosso projeto (de localhost para algum outro endereco), adicionamos no path o id do recurso que está sendo criado.

21. Testamos com Postaman
Importante definir o cabeçalho Content-Type para indicar o tipo de conteúdo que está sendo enviado ao servidor.
A informação vai no body em formato json

22. Validar informações que recebemos do client (ver se temos todas as informações que deveriam ser passadas pelo cliente). Vamos usar as anotations do Bean Validation na classe TopicoForm

@Valid avisa o Spring que a validação deve ser aplicada nos parãmetros do metodo cadastrar e anotações na classe(@NotNul @NotEmpty Length(min=10)) na classe TopicoForm indicam os tipos de validação que devem ser aplicados para cada atributo (podem ser criadas validações personalizadas)

23. Qnd fazemos a requisição com dados inválidos (Bean Validation) retorna um json detalhado do erro. Criamos um interceptador de exceptions para gerenciar esses erros (ErrosValidacaoHandler)
Vamos criar uma classe para gerenciar os erros de validação e devolver um dto da mensagem que definimos
ErroValidacaoHandler e ErroValidacaoDto.

Para tratar os erros de validação do Bean Validation e personalizar o JSON, que será devolvido ao cliente da API, com as mensagens de erro, devemos criar um método na classe @RestControllerAdvice e anotá-lo com @ExceptionHandler e @ResponseStatus.git

Para simular uma mensagem vindo em um outro idioma basta acrescentar no header o parâmetro:
Accept-Language: en-US (para ingles EUA, por exemplo)

24. Adicionado método para detalhar um topico (buscar um id unico) no TopicoController

25. Personalizar o DTO devolvido pelo método detalhar (flexibilidade para retornar mais ou menos informações)

26. Adicionar um método para atualizar informações de um tópico

PUT - Sobrescrever o recurso inteiro (geralmente usamos esse!)
PATCH - pequena atualização (só um ou poucos campos)

27. Adicionar método para remover um tópico pelo id

Nota: Importante adicionar o @Transational em todos os métodos que envolvem adicionar, editar e deletar um recurso no banco de dados


28. Tratar casos que retornam 404(Not found) em vez de 500, que é o que retorna caso não encontre o id informado. Para isso, basta trocar o método getOne() por findById(), trocar o retorno esperado por um Optional e avaliar se existe usando o método isPresent(); 
Nota: O método getOne lança uma exception quando o id passado como parâmetro não existir no banco de dados;
     O método findById retorna um objeto Optional<>, que pode ou não conter um objeto.
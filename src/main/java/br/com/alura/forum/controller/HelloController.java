package br.com.alura.forum.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller //Essa annotation indica para o Spring que a classe é um controller (o spring passa a gerenciar)
public class HelloController {

    @RequestMapping("/") //Essa annotation diz para o Spring o endereço da requisição que estamos mapeando
    @ResponseBody //Indica que o retorno do método não é um direcionamento para uma página, e sim queremos que a string seja exibida no navegador
    public String printHello() {
        return "Hello World!";
    }
}

package br.com.alura.forum.controller.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
public class ErroValidacaoHandler {

    @Autowired
    private MessageSource messageSource; //Classe ajuda a coletar mensagens de erro do spring (ajuda a devolver mensagens em diferentes idiomas

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    //Para indicar que não é pra retornar 200 (pq o Spring considera tratado o erro
    @ExceptionHandler(MethodArgumentNotValidException.class)
    //Anotation no método que será executado quando for capturada uma Exeption da classe indicada entre parênteses
    public List<ErroValidacaoDto> handle(MethodArgumentNotValidException exception) {
        List<ErroValidacaoDto> dto = new ArrayList<>();
        List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();
        fieldErrors.forEach(e -> { // Lambda para cada erro devolvo uma instancia do objeto ErroValidacaoDto(campo,erro)
            String mensagem = messageSource.getMessage(e, LocaleContextHolder.getLocale()); //Pega o idioma definido localmente e devolve a mensagem personalizada
            ErroValidacaoDto erro = new ErroValidacaoDto(e.getField(), mensagem);
            dto.add(erro);
        });
        return dto;
    }
}

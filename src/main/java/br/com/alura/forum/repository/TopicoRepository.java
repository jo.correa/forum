package br.com.alura.forum.repository;

import br.com.alura.forum.model.Topico;
import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository //Como a classe tem o nome é opcional essa annotation
public interface TopicoRepository extends JpaRepository<Topico, Long> {

    List<Topico> findByTitulo(String titulo);

    //    List<Topico> findByCurso_Nome(String nomeCurso); //Quando quisermos fitrar algum atributo que esteja dentro de um atributo da minha classe(relacionamento), basta adicionar o _ assim o Spring entende que o atributo pertence a entidade Curso dentro da entidade Topico. Outra forma é escrever apenas CursoNome (sem o _, algumas pessoas preferem o _ pra dizer que se trata de uma navegação, e para não confundir com atributos homônimos da entidade principal)

    // @Query("SELECT t FROM Topico t WHERE t.curso.nome = :nomeCurso" ) // Essa anotacao serve para pesonamizar o nome do metodo, uma vez que não foi adotado o padrão de nomenclatura
    // List<Topico> filtrarPorNome(@Param("nomeCurso") String nomeCurso); // O @Param serve para que o Spring reconheça o nomeCurso como parametro da query
}
